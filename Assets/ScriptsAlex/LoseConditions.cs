using UnityEngine;
using System.Collections;

public class LoseConditions : MonoBehaviour {
	
	public Transform explosionFire;
	private Transform tempFire;
	
	void Start () {
	
	}
	
	void OnTriggerEnter(Collider other)
	{
		this.renderer.enabled = false;
		
		if(PlayerController.numLives > 0)
			PlayerController.numLives -= 1;
		
		if(this.renderer.enabled == false)
			tempFire = (Transform)Instantiate(explosionFire,new Vector3(this.transform.position.x,this.transform.position.y,this.transform.position.z + 10),Quaternion.identity);
	}
	
	// Update is called once per frame
	void Update () {
	
		if(this.renderer.enabled)
			if(tempFire)
				Destroy(tempFire.gameObject);
		
	}
}
