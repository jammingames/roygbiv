using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SpawnPrefab : MonoBehaviour {
	
	public Transform prefab;
	public int numPrefabs;
	public float spawnDistance, recycleDist;
	public float trailDistance = 1.0f;
	public bool spawnOnObject;
	public bool resize;
	public bool trailSpawn = false;
	public float sizeOfSqrSpawn;
	
	private Vector2 minScreenDist,maxScreenDist;
	
	private Vector3 spawnPos;
	private Transform objectPosition;
	private Queue<Transform> prefabQueue;
	
	private float tempSpawnZ;
	
	void Awake()
	{
		GameEventManager.GameStart += OnGameStart;
		GameEventManager.GameOver += OnGameOver;
		enabled = false;
	}
	
	// Use this for initialization
	void OnEnable () 
	{
		
	
	}
	
	private void Recycle()
	{
		tempSpawnZ += spawnDistance;
		Vector3 tempScale = new Vector3(Random.Range(0.5f,3),Random.Range(0.5f,3),Random.Range(0.5f,3));
		spawnPos = new Vector3(Random.Range(minScreenDist.x,maxScreenDist.x),Random.Range(minScreenDist.y,maxScreenDist.y), tempSpawnZ);
		
		Transform tempPrefab = prefabQueue.Dequeue();
		if(resize)
			tempPrefab.transform.localScale = tempScale;
		
		tempPrefab.renderer.enabled = true;
		tempPrefab.transform.position = spawnPos;
		prefabQueue.Enqueue(tempPrefab);
	}
	
	private void Recycle(float dist)
	{
		tempSpawnZ += spawnDistance;
		Vector3 newSpawnPos = spawnPos;
		Vector3 tempScale = new Vector3(Random.Range(0.5f,3),Random.Range(0.5f,3),Random.Range(0.5f,3));
		newSpawnPos= new Vector3(newSpawnPos.x + Random.Range(-dist,dist),newSpawnPos.y + Random.Range(-dist,dist), tempSpawnZ);
		
			if (newSpawnPos.x <= minScreenDist.x/1.3f) 
					newSpawnPos.x += dist;
				if (newSpawnPos.x >= maxScreenDist.x/1.3f) 
					newSpawnPos.x -= dist;
					
				if (newSpawnPos.y <= minScreenDist.y/5f) 
					newSpawnPos.y += dist;
				if (newSpawnPos.y >= maxScreenDist.y/5f) 
					newSpawnPos.y -= dist;
					
			
		spawnPos = newSpawnPos;
		
		Transform tempPrefab = prefabQueue.Dequeue();
		if(resize)
			tempPrefab.transform.localScale = tempScale;
		
		tempPrefab.renderer.enabled = true;
		tempPrefab.transform.position = spawnPos;
		prefabQueue.Enqueue(tempPrefab);
	}
	
	private void Recycle(Vector3 position)
	{
		tempSpawnZ += spawnDistance;
		Vector3 tempScale = new Vector3(Random.Range(0.5f,3),Random.Range(0.5f,3),Random.Range(0.5f,3));
		
		spawnPos = new Vector3(position.x,position.y, tempSpawnZ);
		
		Transform tempPrefab = prefabQueue.Dequeue();
		if(resize)
			tempPrefab.transform.localScale = tempScale;
		
		tempPrefab.renderer.enabled = true;
		tempPrefab.transform.position = spawnPos;
		prefabQueue.Enqueue(tempPrefab);
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(prefabQueue.Peek().transform.position.z + recycleDist < PlayerController.distTraveled && !trailSpawn && !spawnOnObject)
		{
			Recycle();
		}
		else if(prefabQueue.Peek().transform.position.z + recycleDist < PlayerController.distTraveled && trailSpawn)	
		{
			Recycle(trailDistance);
		}
		else if (prefabQueue.Peek().transform.position.z + recycleDist < PlayerController.distTraveled && spawnOnObject)
		{
			Recycle(objectPosition.position);
		}
	
	}
	
	void OnGameStart()
	{
		enabled = true;
		if (spawnOnObject)
		{
			objectPosition = GameObject.Find("Spawner").transform;
			
		}
			
		maxScreenDist.y = sizeOfSqrSpawn;
		maxScreenDist.x = sizeOfSqrSpawn;
		minScreenDist.y = -sizeOfSqrSpawn;
		minScreenDist.x = -sizeOfSqrSpawn;
		tempSpawnZ = PlayerController.distTraveled;
		prefabQueue = new Queue<Transform>(numPrefabs);
		spawnPos = new Vector3(Random.Range(minScreenDist.x,maxScreenDist.x),Random.Range(minScreenDist.y,maxScreenDist.y), tempSpawnZ);
		for(int i = 0; i < numPrefabs; i++)
		{
		
			tempSpawnZ += spawnDistance;
			
			
			if (trailSpawn) 
			{
				float dist = trailDistance;
				Vector3 newSpawnPos = spawnPos;
				newSpawnPos= new Vector3(newSpawnPos.x + Random.Range(-dist,dist),newSpawnPos.y + Random.Range(-dist,dist), tempSpawnZ);
				
				if (newSpawnPos.x <= minScreenDist.x/1.3f) 
					newSpawnPos.x += dist;
				if (newSpawnPos.x >= maxScreenDist.x/1.3f) 
					newSpawnPos.x -= dist;
					
				if (newSpawnPos.y <= minScreenDist.y/5f) 
					newSpawnPos.y += dist;
				if (newSpawnPos.y >= maxScreenDist.y/2f) 
					newSpawnPos.y -= dist;
					
				spawnPos = newSpawnPos;
		
			}
			else if (spawnOnObject)
				spawnPos = new Vector3(objectPosition.position.x, objectPosition.position.y, tempSpawnZ);
			else
				spawnPos = new Vector3(Random.Range(minScreenDist.x,maxScreenDist.x),Random.Range(minScreenDist.y,maxScreenDist.y), tempSpawnZ);
			Transform tempPrefab = (Transform)Instantiate(prefab);
		
			if(resize)
			{
				Vector3 tempScale = new Vector3(Random.Range(0.5f,3),Random.Range(0.5f,3),Random.Range(0.5f,3));
				tempPrefab.transform.localScale = tempScale;
			}
			
			tempPrefab.transform.position = spawnPos;
			prefabQueue.Enqueue(tempPrefab);
		}
	}
	
	void OnGameOver()
	{
		enabled = false;
	}
	
}
