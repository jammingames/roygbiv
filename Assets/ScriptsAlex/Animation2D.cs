using UnityEngine;
using System.Collections;

public class Animation2D : MonoBehaviour {
	
	private class AnimationClipData
	{
		int startFrame,endFrame, fps;
		
	}
	
	public int xTiles, yTiles, numberOfTiles;
	public bool squareTileSheet = false, loop = false;
	
	private float tileHeight, tileWidth;

	public int fps;
	private float _fps, timer;
	private int currTile, numTiles;
	
	public float testTimeThingy = 1.0f;
	
	private Vector2[] Tiles;
	// Use this for initialization
	void Start () {
	
		tileHeight = 1f/yTiles;
		tileWidth = 1f/xTiles;
		
		if(squareTileSheet == true)
			numTiles = xTiles*yTiles;
		
		else
			numTiles = numberOfTiles;
		
		_fps = 1.0f/fps;
		
		currTile = 0;
		
		Tiles = new Vector2[numTiles];
		StoreTileSheet();
		
	}
	
	private void StoreTileSheet()
	{
		int allTheTiles = 0;
		for(float y = yTiles-1; y >= 0; y--)
		{
			for(float x = 0; x < xTiles; x++)
			{
				float xPos = x*tileWidth;
				float yPos = y*tileHeight;
				Vector2 tempPos = new Vector2(xPos,yPos);	
				Tiles[allTheTiles] = tempPos;
				allTheTiles++;

				if(allTheTiles >= numTiles)
					break;
			}
	
		}
	}
	
	private Vector2 solveForFrames(int frameCount)
	{
		var tempVect2 = new Vector2(0,0);
		if(frameCount > xTiles)
		{
			if(frameCount%xTiles == 0)
				tempVect2.x = xTiles;
			else if(frameCount%xTiles == xTiles)
				tempVect2.x = 0;
			
			else
				tempVect2.x = frameCount%xTiles;
		}
		
		else
			tempVect2.x = frameCount;
		
		tempVect2.y = (Mathf.Floor(frameCount/yTiles)+1);
		
		return tempVect2;
	}
	
	// Update is called once per frame
	void Update () {

		if(timer > 0.0f)
			timer -= Time.deltaTime;
		if(timer <= 0.0f)
			timer = 0.0f;
		if(timer == 0.0f)
			NextFrame();
	
	}
	
	private void NextFrame()
	{
		if(loop)
		{
			if(currTile < Tiles.Length -1)
				currTile++;
		
			else
				currTile = 0;
			}
		
			else
			{
				if(currTile < Tiles.Length -1)
					currTile++;
			}
		
		renderer.material.mainTextureOffset = Tiles[currTile];
		timer = _fps;
	}
}