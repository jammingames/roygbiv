using UnityEngine;
using System.Collections;

public class Score : MonoBehaviour {
	
	private float currScore = 0;
	
	private float currMultiplier = 1;
	
	private float lengthOfSurvival = 0;
	
	// Use this for initialization
	void Start () {
	
		
		
	}
	
	// Update is called once per frame
	void Update () {
		lengthOfSurvival += Time.deltaTime;
		
		currScore += Mathf.Abs((PlayerController.distTraveled/lengthOfSurvival)*currMultiplier);
		
		print(currScore);
	}
}
