using UnityEngine;
using System.Collections;

public class GameStartGUI : MonoBehaviour {

	public GUIText gameOverText, gameStartText, instructionText;
	
	void Start () 
	{
		GameEventManager.GameStart += GameStart;
		GameEventManager.GameOver += GameOver;
		gameOverText.enabled = false;
	
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (Input.GetKeyDown(KeyCode.Space))
			GameEventManager.TriggerGameStart();
	}
	
	
	void GameStart () 
	{
		gameOverText.enabled = false;
		instructionText.enabled = false;
		gameStartText.enabled = false;
		enabled = false;
	}
	
	void GameOver () 
	{
		gameOverText.enabled = true;
		instructionText.enabled = true;
		enabled = true;
	}
}
