using UnityEngine;
using System.Collections;

public class SpawnEnemy : MonoBehaviour {
	
	public GameObject enemy;
	public float delay = 1.0f;
	public bool canSpawn;
	
	void Start () {
	StartCoroutine("SpawnEnemies", 9);
	}
	
	
	void Update () {
	
	}
	
	
	
	IEnumerator SpawnEnemies(float number)
	{
		int counter = 0;
		while (counter < number)
		{
			Vector3 randomPos = new Vector3(Random.Range(-30,30), Random.Range(-10,10), 0);
			Instantiate(enemy, transform.position + randomPos, Quaternion.identity);
			yield return new WaitForSeconds(delay);
		}
	}
}
