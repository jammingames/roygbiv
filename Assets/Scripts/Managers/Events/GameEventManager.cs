using System.Collections;
using UnityEngine;

public static class GameEventManager {
	
	public delegate void GameEvent();
	public static event GameEvent GameStart, GameOver;
	
	public static void TriggerGameStart(){
		if(GameStart != null){
			Debug.Log("stuff");
			GameStart();
		}
	}
	
	
	
	public static void TriggerGameOver(){
		if(GameOver != null){
			GameOver();
		}
	}
}
