using UnityEngine;
using System.Collections;

public class ShipUpgrade : MonoBehaviour {
	
	public Transform playerShip;
	
	public float colorChangeTime;
	
	private Color origShipColor, lerpColor;
	
	private int colorBS;
	
	private float colorTimer;
	
	private float lerpTimer;
	
	private bool lerpingColor = false;

	// Use this for initialization
	void Start () 
	{
		colorBS = 0;
		
		colorTimer = 0;
		lerpTimer = 0;
		origShipColor = playerShip.renderer.material.color;
		lerpColor = playerShip.renderer.material.color;
	}
	
	private void NextColor()
	{
		switch(colorBS)
		{
			
		case 0:
			lerpColor = Color.red;
			colorBS += 1;
			break;
			
		case 1:
			origShipColor = Color.red;
			lerpColor = Color.red+Color.green;
			colorBS += 1;
			break;
			
		case 2:
			origShipColor = Color.red+Color.green;
			lerpColor = Color.yellow;
			colorBS += 1;
			break;
			
		case 3:
			origShipColor = Color.yellow;
			lerpColor = Color.green;
			colorBS += 1;
			break;
			
		case 4:
			origShipColor = Color.green;
			lerpColor = Color.blue;
			colorBS += 1;
			break;
			
		case 5:
			origShipColor = Color.blue;
			lerpColor = Color.magenta;
			colorBS += 1;
			break;
			
		case 6:
			origShipColor = Color.magenta;
			lerpColor = Color.red+Color.blue;
			colorBS += 1;
			break;
			
		case 7:
			origShipColor = Color.red+Color.blue;
			lerpColor = Color.red;
			colorBS = 1;
			break;
			
		}
		
	}
	

	// Update is called once per frame
	void Update () 
	{
		lerpTimer = colorTimer/colorChangeTime;

		//Roy G. Biv motherfuckers
		if(colorTimer < colorChangeTime)
			colorTimer += Time.deltaTime;
		
		if(colorTimer >= colorChangeTime)
			colorTimer = colorChangeTime;
		
		if(colorTimer == colorChangeTime)
		{
			lerpingColor = true;
			NextColor();
			colorTimer = 0;
			lerpTimer = 0;
		}
		
		
		playerShip.renderer.material.color = Color.Lerp(origShipColor, lerpColor, lerpTimer);
	}
}
