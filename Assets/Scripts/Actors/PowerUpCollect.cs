using UnityEngine;
using System.Collections;

public class PowerUpCollect : MonoBehaviour {

	public Transform explosionFire;
	private Transform tempFire;
	// Use this for initialization
	void Start () {
	
	}
	
	void OnTriggerEnter(Collider other)
	{
		this.renderer.enabled = false;
		
		
		if(this.renderer.enabled == false)
			tempFire = (Transform)Instantiate(explosionFire,new Vector3(this.transform.position.x,this.transform.position.y,this.transform.position.z),Quaternion.identity);
			tempFire.parent = other.transform;
		
							
	}
	
	// Update is called once per frame
	void Update () {
	
		if(tempFire)
			if(this.renderer.enabled)
				Destroy(tempFire.gameObject);
		
	}
}
