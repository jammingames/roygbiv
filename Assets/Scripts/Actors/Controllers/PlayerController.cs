using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {

	Vector3 inputAxis;
	public Camera camera;
	public float speed = 1.0f;
	public float angle = 1.0f;
	private Vector3 lastPosition;
	private Vector3 positionChange;
	private Vector3 mousePos;
	private float velocity;
	
	public static int numLives;
	
	public static float distTraveled;
	
	public float rotationSpeed = 5;
	
	void Awake()
	{
		GameEventManager.GameOver += OnGameOver;
	}
	
	void Start () 
	{
		inputAxis = new Vector3(0,0,0);
		numLives = 50;
		
	}
	
	void Update () 
	{
		distTraveled = transform.parent.transform.localPosition.z;
		
		lastPosition = transform.position;
		
				Ray ray = camera.ScreenPointToRay(Input.mousePosition);
				RaycastHit hit;
			
			if(PlayerController.numLives > 0)
			{
				if (Physics.Raycast(ray, out hit)) 
				{
					mousePos = new Vector3(hit.point.x,hit.point.y,transform.position.z);
					transform.position = Vector3.Lerp(transform.position, mousePos, Time.deltaTime);
				}
			}
			else
				GameEventManager.TriggerGameOver();	
		
		
		
		positionChange = transform.position - lastPosition;
		velocity = positionChange.magnitude;
		angle = velocity * 3;
		if (positionChange.x <= 0) 
			angle *= -1;
		if (mousePos.x - transform.position.x <= 0.3f)
			transform.RotateAround(transform.forward, GetAngle(transform.position, mousePos)*Time.deltaTime);
		if (mousePos.x - transform.position.x >= 0.3f)
			transform.RotateAround(transform.forward, GetAngle(transform.position, mousePos)*Time.deltaTime *-1);
	}
	
	float GetAngle(Vector3 from, Vector3 to)
	{
		from = new Vector3(from.x, from.y, 20);
		to = new Vector3(to.x, to.y, 20);
		return Vector3.Angle(from, to);
	}
	
	
		
				
	
	void SmoothLookAt(Vector3 target, float smooth)
	{
   		Vector3 dir = target - transform.position + transform.up;
    	Quaternion targetRotation = Quaternion.LookRotation(dir);
		targetRotation.x = 0;
		targetRotation.y = 0;
   		transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, Time.deltaTime * smooth);
	}
	
	void OnGameOver()
	{
		distTraveled = 0;
		numLives = 50;
	}
}

