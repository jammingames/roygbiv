using UnityEngine;
using System.Collections;

public class HitSound : MonoBehaviour {

	
	public AudioClip[] hitSound;
	
	void OnTriggerEnter(Collider other)
	{
		if(hitSound.GetLength(0)>0)
		{
		 	AudioSource.PlayClipAtPoint(hitSound[Random.Range(0,hitSound.GetLength(0))], other.transform.position);
		}
	}
}
