using UnityEngine;
using System.Collections;

public class MoveForwards : MonoBehaviour {
	
	public static float forwardSpeed = 20.0f;
	public float timeBeforeSpeedChange;
	
	private float speedIncTimer;
	
	void Awake() 
	{
		GameEventManager.GameStart += OnGameStart;
		GameEventManager.GameOver += OnGameOver;
		enabled = false;
	}
	
	
	void Start () {
		
		speedIncTimer = timeBeforeSpeedChange;

	}
	
	private void IncreaseSpeed()
	{
		forwardSpeed += 10;
		ResetTimer();
	}
	
	private void ResetTimer()
	{
		speedIncTimer = timeBeforeSpeedChange;
	}
	
	
	// Update is called once per frame
	void Update () 
	{
		if(speedIncTimer > 0)
			speedIncTimer -= Time.deltaTime;
		if(speedIncTimer <= 0)
			speedIncTimer = 0;
		if(speedIncTimer == 0)
			IncreaseSpeed();
		
		
		
		if(PlayerController.numLives > 0)
			transform.Translate(0,0,forwardSpeed * Time.deltaTime);
	}
	
	void OnGameStart()
	{
		print("moveforwards started");
		enabled = true;
	}
	
	void OnGameOver()
	{
		transform.position = new Vector3(0,0,-10);
		forwardSpeed = 20.0f;
		enabled = false;
	}
	
}
